<?php

return [
    'customer' => array(
        'name' => env('CUSTOMER_NAME'),
        'occupation' => env('CUSTOMER_OCCUPATION'),
        'payment_details' => env('CUSTOMER_PAYMENT_DETAILS'),
        'address' => env('CUSTOMER_ADDRESS'),
        'zip' => env('CUSTOMER_ZIP'),
        'phone' => env('CUSTOMER_PHONE'),
    )
];
