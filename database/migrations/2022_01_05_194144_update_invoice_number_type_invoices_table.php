<?php

use App\Models\Invoice;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateInvoiceNumberTypeInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->string('invoice_number')->change();
        });

        $invoices = Invoice::all();

        foreach ($invoices as $invoice) {
            $updateInvoiceNumber = $invoice->invoice_number . '-' . "2021";
            $invoice->setAttribute('invoice_number', $updateInvoiceNumber);
            $invoice->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
