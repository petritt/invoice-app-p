<?php

use App\Models\Invoice;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDueDateSendInvoiceToInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->bigInteger('invoice_total')->change()->description('in cents, including tax');
            $table->bigInteger('tax')->default(0)->after('invoice_total')->description('in cents');
            $table->bigInteger('discount')->default(0)->after('tax')->description('in cents');
            $table->char('currency', 3)->after('discount');
            $table->char('status', 16)->nullable()->after('currency');
        });

        $invoices = Invoice::all();

        foreach ($invoices as $invoice) {
            $updateInvoiceNumber = $invoice->invoice_total * 100;
            $invoice->setAttribute('invoice_total', $updateInvoiceNumber);
            $invoice->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->dropColumn(['tax', 'discount', 'currency', 'status']);
        });
    }
}
