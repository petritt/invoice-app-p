<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Client;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProductsController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            /** @var Product $products */
            $products = Product::get(["id", "product_name", "product_description"]);
            return response()->json($products);
        } catch (\Exception $e) {
            return response()->json(["messages" => prepare_response('danger', $e->getMessage())], 500);
        }
    }

    /**
     * @param int $client_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductsByid(int $client_id)
    {
        try {
            /** @var Client $client */
            $client = Client::findOrFail($client_id);
            return response()->json($client);
        } catch (\Exception $e) {
            return response()->json(["messages" => prepare_response('danger', $e->getMessage())], 500);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($productId)
    {
        try {
            /** @var Activity $activities */
            $activities = Activity::where('client_id', $productId)
                ->get(['id', 'date', 'duration']);
            return response()->json($activities);
        } catch (\Exception $e) {
            return response()->json(["messages" => prepare_response('danger', $e->getMessage())], 500);
        }
    }

    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            Client::firstOrCreate($request->all());
            return response()->json(["messages" => prepare_response('success', 'The client was successfully created')]);
        } catch (\Exception $e) {
            return response()->json(["messages" => prepare_response('danger', $e->getMessage())], 500);
        }
    }
}
