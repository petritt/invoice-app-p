<?php

namespace App\Http\Controllers;

use App\Jobs\SendInvoice;
use App\Models\Activity;
use App\Models\Client;
use App\Models\Invoice;
use App\Models\InvoiceActivity;
use App\Models\ProductClientPricing;
use App\Notifications\InvoiceMail;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class InvoicesController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            /** @var Invoice $invoices */
            $invoices = Invoice::with('client')
                ->get(["id", "invoice_number", "invoice_due_date", "client_id", "invoice_total"]);

            return response()->json($invoices);
        } catch (\Exception $e) {
            return response()->json(["messages" => prepare_response('danger', $e->getMessage())], 500);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {

    }

    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        try {

            $data = $request->all();
            $invoiceActivities = $data['items'];
            unset($data['items']);

            $data["invoice_due_date"] = prepare_date_format($data["invoice_due_date"]);

            $invoiceModel = Invoice::firstOrCreate($data);

            foreach ($invoiceActivities as $invoiceActivity) {
                $invoiceModel->addInvoiceActivities(['invoice_id' => $invoiceModel->id, 'activity_id' => $invoiceActivity['id']]);
            }

            return response()->json(["messages" => prepare_response('success', 'The activity was successfully created')]);
        } catch (\Exception $e) {
            return response()->json(["messages" => prepare_response('danger', $e->getMessage())], 500);
        }
    }

    public function edit($invoiceId)
    {

        /** @var Invoice $invoice */
        $invoice = Invoice::findOrFail($invoiceId)->complete();

//        usort($invoice['invoice_activity'], function ($a, $b) {
//            return ($a['date'] < $b['date']) ? -1 : 1;
//        });

        return response()->json($invoice);
    }

    public function generatePDF($invoiceId)
    {
        /** @var Invoice $invoice */
        $invoice = Invoice::findOrFail($invoiceId);
        $invoice->download();
    }

    public function lastInvoiceNumber()
    {
        try {

            $record = Invoice::latest()->first()->invoice_number;
            $expNum = explode('-', $record);

            if (date("Y") !== $expNum[1]) {
                $nextInvoiceNumber = '001-' . date('Y');
            } else {
                //increase 1 with last invoice number
                $numb = str_pad(($expNum[0] + 1), 3, "0", STR_PAD_LEFT);
                $nextInvoiceNumber = $numb . '-' . $expNum[1];
            }

            return response()->json($nextInvoiceNumber);

        } catch (\Exception $e) {
            return response()->json(["messages" => prepare_response('danger', $e->getMessage())], 500);
        }
    }

    /**
     * @param int $invoiceId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInvoiceAsPdf(int $invoiceId)
    {
        try {
            /** @var Invoice $invoice */
            $invoice = Invoice::findOrFail($invoiceId);

            /** @var Client $client */
            $client = Client::findOrFail($invoice->client_id);

            $invoice->send($client);


            return response()->json(["messages" => prepare_response('success', 'The email was send')]);

        } catch (\Exception $e) {
            return response()->json(["messages" => prepare_response('danger', $e->getMessage())], 500);
        }
    }


    public function sendReminder(int $invoiceId)
    {
        try {
            /** @var Invoice $invoice */
            $invoice = Invoice::findOrFail($invoiceId);

            /** @var Client $client */
            $client = Client::findOrFail($invoice->client_id);

            $invoice->sendReminder($client);


            return response()->json(["messages" => prepare_response('success', 'The email was send')]);

        } catch (\Exception $e) {
            return response()->json(["messages" => prepare_response('danger', $e->getMessage())], 500);
        }
    }
}
