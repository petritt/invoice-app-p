<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Invoice;
use App\Report;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('welcome');
    }

    public function getDashboardData()
    {
        try {
            $data = [];
            /** @var Invoice $invoices */
            $invoices = Invoice::get(['invoice_total', 'invoice_due_date']);
            /** @var Client $clinets */
            $clinets = Client::get(['client_name']);

            $report = new Report($invoices);

            $data['total_invoices'] = count($invoices);
            $data['total_revenue'] = $report->getTotalRevenue();
            $data['total_clients'] = count($clinets);
            $data['total_revenue_by_months'] = $report->yearly();

            return response()->json($data);
        } catch (\Exception $e) {
            $responseMessages = [["type" => "danger", "message" => $e->getMessage(), $e->getTrace()]];
            return response()->json(["messages" => $responseMessages], 500);
        }
    }

}
