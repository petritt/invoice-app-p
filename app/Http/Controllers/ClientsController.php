<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Client;
use App\Models\Product;
use App\Models\ProductClientPricing;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ClientsController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        /** @var Client $clients */
        $clients = Client::get(["id", "client_name", "zip", "address", "city", "email", "price", "is_active"]);
        return response()->json($clients);
    }

    /**
     * @param int $client_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getClientByid(int $client_id)
    {
        try {
            return response()->json(Client::findOrFail($client_id));
        } catch (\Exception $e) {
            return response()->json(["messages" => prepare_response('danger', $e->getMessage())], 500);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $dates = json_decode($request->query('dates'));

        if ($dates->ending_date === null) {
            $dates->starting_date = date('Y-m-01', strtotime($dates->starting_date));
            $dates->ending_date = date('Y-m-t', strtotime($dates->starting_date));
        }

        if ($dates->ending_date === 'last') {
            $dates->starting_date = Carbon::now()->subMonth()->startOfMonth()->toDateString();
            $dates->ending_date = Carbon::now()->subMonth()->endOfMonth()->toDateString();
        }

        if ($dates->ending_date === 'last_3') {
            $dates->starting_date = Carbon::now()->startOfMonth()->subMonth(2)->toDateString();
            $dates->ending_date = Carbon::now()->endOfMonth()->toDateString();
        }

        /** @var Activity $activities */
        $activities = Activity::where('client_id', $id)
            ->whereBetween('date', array($dates->starting_date, $dates->ending_date))
            ->get(['id', 'product_id', 'date', 'duration']);


        /** @var ProductClientPricing $productsPricing */
        $productsPricing = ProductClientPricing::with('pricing')->where('client_id', $id)->get();
        $productPricingByProductId = [];
        foreach ($productsPricing as $productPricing) {
            $productPricingByProductId[$productPricing->product_id] = $productPricing->pricing->price;
        }

        $activities = $activities->map(function ($activity) use ($productPricingByProductId) {
            $activity->product_name = "";
            $product = $activity->product;
            if (isset($productPricingByProductId[$product->id])) $activity->price = $productPricingByProductId[$product->id];
            $activity->product_name = $product->product_name;

            return $activity;
        });


        return response()->json($activities);
    }

    /**
     * @param Request $request
     * @param integer $client_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getActivityByClient(Request $request, int $client_id)
    {
        $filterBy = $request->query('dates');

        /** @var Client $client */
        $client = Client::findOrFail($client_id);

        /** @var Activity $activities */
        $activities = Activity::where('client_id', $client_id);

        if ($filterBy === 'last') {
            $fromDate = Carbon::now()->subMonth()->startOfMonth()->toDateString();
            $tillDate = Carbon::now()->subMonth()->endOfMonth()->toDateString();

            $activities->whereBetween('date', [$fromDate, $tillDate]);
        }

        if ($filterBy === 'current') {
            $activities->where('date', '>=', Carbon::now()->firstOfMonth()->toDateTimeString());
        }

        $activities = $activities->get(['id', 'product_id', 'date', 'duration']);

        $activities = $activities->map(function ($activity) {
            $activity->product_name = "";
            $product = $activity->product;
            $activity->product_name = $product->product_name;

            return $activity;
        });

        $activitiesByClient = [];
        $activitiesByClient['activitites'] = $activities;
        $activitiesByClient['client'] = $client;
        return response()->json($activitiesByClient);
    }

    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $requestData = $request->all();

            /** @var Client $clientModel */
            $clientModel = Client::firstOrNew(['id' => $requestData['id'] ?? null]);
            $clientModel->setAttribute('client_name', $requestData['client_name']);
            $clientModel->setAttribute('zip', $requestData['zip']);
            $clientModel->setAttribute('address', $requestData['address']);
            $clientModel->setAttribute('city', $requestData['city']);
            $clientModel->setAttribute('email', $requestData['email']);
            $clientModel->setAttribute('price', $requestData['price']);
            $clientModel->setAttribute('is_active', to_boolean($requestData['is_active']));
            $clientModel->save();

            return response()->json(["messages" => prepare_response('success', 'The client was successfully created')]);

        } catch (\Exception $e) {
            return response()->json(["messages" => prepare_response('danger', $e->getMessage())], 500);
        }
    }
}
