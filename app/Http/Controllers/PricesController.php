<?php

namespace App\Http\Controllers;

class PricesController extends Controller
{

    const availablePrices = [
        "16", "17", "18", "19", "20"
    ];

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {
        try {
            return response()->json(self::availablePrices);
        } catch (\Exception $e) {
            return response()->json(["messages" => prepare_response('danger', $e->getMessage())], 500);
        }
    }
}
