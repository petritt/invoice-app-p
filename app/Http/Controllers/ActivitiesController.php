<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Client;
use Illuminate\Http\Request;

class ActivitiesController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {

    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {

    }

    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        try {

            $activities = $request->all();

            foreach ($activities as $activity) {
                if ($activity['id'] == null) {
                    if (isset($activity['product_name'])) unset($activity['product_name']);
                    Activity::firstOrCreate($activity);
                }
            }

            return response()->json(["messages" => prepare_response('success','The activity was successfully created')]);

        } catch (\Exception $e) {
            return response()->json(["messages" => prepare_response('danger', $e->getMessage())], 500);
        }
    }
}
