<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Client;

class SendLatestInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'i:send-latest-invoice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a clinet their most recently generated invoice';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = $this->client();

        $client->sendLastInvoice();

        $this->info("Invoice delivered to {$client->email}");

        return 0;
    }

    protected function client()
    {
        $email = $this->ask("What is the email address for the client?");

        return Client::where(['email' => $email])
            ->firstOrFail();
    }
}
