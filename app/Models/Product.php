<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        "product_name",
        "product_description",
    ];

    public function productClientPricing(): \Illuminate\Database\Eloquent\Relations\hasOne
    {
        return $this->hasOne(ProductClientPricing::class, 'product_id');
    }

    public function activity(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Activity::class, 'product_id');
    }

}
