<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceActivity extends Model
{

    protected $table = "invoices_activities";

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        "invoice_id",
        "activity_id",
    ];

    public function invoice(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Invoice::class, 'invoice_id', 'id');
    }

    public function activity(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Activity::class, 'activity_id', 'id');
    }

}
