<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductClientPricing extends Model
{

    protected $table = "products_clients_pricing";

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        "client_id",
        "product_id",
        "pricing_id",
    ];

    public function products(): \Illuminate\Database\Eloquent\Relations\belongsTo
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function pricing(): \Illuminate\Database\Eloquent\Relations\belongsTo
    {
        return $this->belongsTo(Pricing::class, 'pricing_id');
    }

    public function scopeByClient($query, $clientId)
    {
        return $query->where('client_id', $clientId);
    }



}
