<?php

namespace App\Models;

use App\Jobs\SendInvoice;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Client extends Model
{

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        "client_name",
        "zip",
        "address",
        "city",
        "email",
        "price",
        "is_active"
    ];

    protected $casts = [
        'is_active' => 'boolean',
    ];

    public function activity(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Activity::class, 'client_id');
    }

    public function invoice(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Invoice::class, 'client_id');
    }

    public function latestInvoice()
    {
        return $this->hasMany(Invoice::class, 'client_id')->latest();
    }

    public function sendLastInvoice()
    {
        $invoice = $this->latestInvoice()->first();
        SendInvoice::dispatch($invoice, $this);
    }
}
