<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pricing extends Model
{

    protected $table = "pricing";

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        "price",
    ];

    public function productClientPricing(): \Illuminate\Database\Eloquent\Relations\hasMany
    {
        return $this->hasMany(ProductClientPricing::class);
    }

}
