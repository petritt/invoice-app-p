<?php

namespace App\Models;

use App\Casts\TotalCast;
use App\Jobs\SendInvoice;
use App\Notifications\InvoiceReminder;
use Dompdf\Dompdf;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Symfony\Component\HttpFoundation\Response;

class Invoice extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        "invoice_number",
        "invoice_due_date",
        "client_id",
        "invoice_total",
        "id"
    ];

    protected $casts = [
        'invoice_due_date' => 'date:d.m.Y',
        'invoice_total' => TotalCast::class,
//        'invoice_number'  => InvoiceNummber::class,
    ];

    public function client(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Client::class, 'client_id', 'id');
    }

    public function invoiceActivity(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(InvoiceActivity::class, 'invoice_id');
    }

    /**
     * @param array $invoiceActivities
     */
    public function addInvoiceActivities(array $invoiceActivities)
    {
        $this->invoiceActivity()->firstOrCreate($invoiceActivities);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getInvoiceActivities(): \Illuminate\Support\Collection
    {
        $invoiceActivities = $this->invoiceActivity()->with('activity')->get()->pluck('activity');
        return $this->setActivitiesWithProducts($invoiceActivities);
    }

    /**
     * @param \Illuminate\Support\Collection $invoiceActivities
     * @return \Illuminate\Support\Collection
     */
    private function setActivitiesWithProducts(\Illuminate\Support\Collection $invoiceActivities): \Illuminate\Support\Collection
    {
        foreach ($invoiceActivities as &$activity) {
            $activity->product_name = $activity->product->product_name;

            $priceByProducts = $activity->product->productClientPricing->ByClient($this->client_id)->get();
            if ($priceByProducts->count() > 0) {
                foreach ($priceByProducts as $priceByProduct) {
                    $activity->price = $priceByProduct->pricing->price;
                }
            }

            unset($activity['product']);
        }

        return $invoiceActivities;
    }

    /**
     * @return array
     */
    public function complete(): array
    {
        $invoice = $this->toArray();
        $invoice['client'] = $this->client()->get();
        $invoice['invoice_activity'] = $this->getInvoiceActivities()->toArray();
        return $invoice;
    }

    /**
     * Get the View instance for the invoice.
     *
     * @param array $data
     * @return \Illuminate\View\View
     */
    public function view(array $data = [])
    {
        return View::make('invoice/myPDF', array_merge($data, [
            'invoice' => $this->complete(),
//            'moneyFormatter' => new MoneyFormatter(
//                $this->currency,
//                config('invoicable.locale')
//            ),
        ]));
    }

    /**
     * @param array $data
     * @return Dompdf
     * @throws \Throwable
     */
    public function pdf(array $data = []): Dompdf
    {
        if (!defined('DOMPDF_ENABLE_AUTOLOAD')) {
            define('DOMPDF_ENABLE_AUTOLOAD', false);
        }

        if (file_exists($configPath = base_path() . '/vendor/dompdf/dompdf/dompdf_config.inc.php')) {
            require_once $configPath;
        }

        $dompdf = new Dompdf;
        $dompdf->loadHtml($this->view($data)->render());
        $dompdf->render();

        return $dompdf;
    }

    /**
     * @param array $data
     * @return Response
     * @throws \Throwable
     */
    public function download(array $data = [])
    {
        $filename = $this->invoice_number . '.pdf';

        return new Response($this->pdf($data)->stream(), 200, [
            'Content-Description' => 'File Transfer',
            'Content-Disposition' => 'attachment; filename="' . $filename . '"',
            'Content-Transfer-Encoding' => 'binary',
            'Content-Type' => 'application/pdf',
        ]);
    }

    /**
     * Return Download Path
     * @return string
     */
    public function downloadPath(): string
    {
        return storage_path() . "\app\public\invoices" . DIRECTORY_SEPARATOR . "{$this->invoice_number}.pdf";
    }

    public function send(Client $client)
    {
        SendInvoice::dispatch($this, $client);
    }

    public function sendReminder(Client $client)
    {
        $pdf = $this->pdf();

        $relativePath = "\invoices" . DIRECTORY_SEPARATOR . "{$this->invoice_number}.pdf";
        Storage::disk('public')->put($relativePath, $pdf->output());

        $client->notify(new InvoiceReminder($this));
    }

}
