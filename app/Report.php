<?php

namespace App;

class Report
{
    protected $invoices = [];
    protected $totalRevenue = 0;
    protected $months = [];

    public function __construct($invoices)
    {
        $this->invoices = $invoices;

        for ($i = 1; $i <= 12; $i++) {
            $this->months[$i] = [];
        }
    }

    /**
     * @return array
     */
    public function yearly(): array
    {

        $invoicesByYear = [];

        $invoicesByYear = $this->getInvoicesByYear($invoicesByYear);

        foreach ($invoicesByYear as &$invoiceByMonths) {
            $invoiceByMonths = $invoiceByMonths + $this->months;
            ksort($invoiceByMonths);

            foreach ($invoiceByMonths as &$invoiceByMonth) {
                $invoiceByMonth = array_reduce($invoiceByMonth, function ($previous, $current) {
                    return $previous + self::convertCommaToDot($current);
                }, 0);
            }
        }

        ksort($invoicesByYear);

        return $invoicesByYear;
    }

    protected static function convertCommaToDot($invoice_total): float
    {
        return floatval($invoice_total);
    }

    public function getTotalRevenue()
    {
        $this->totalRevenue = array_reduce($this->invoices->toArray(), function ($previous, $current) {
            return $previous + self::convertCommaToDot($current['invoice_total']);
        });

        return $this->totalRevenue;
    }

    /**
     * @param array $invoicesByYear
     * @return array
     */
    private function getInvoicesByYear(array $invoicesByYear): array
    {
        foreach ($this->invoices as $invoice) {
            $year = $invoice->invoice_due_date->year;
            $month = $invoice->invoice_due_date->month;

            $invoicesByYear[$year][$month][] = $invoice->invoice_total;
        }
        return $invoicesByYear;
    }

}
