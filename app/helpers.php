<?php

if (!function_exists('to_boolean')) {
    /**
     * Convert to boolean
     *
     * @param $booleable
     * @return boolean
     */
    function to_boolean($booleable)
    {
        return filter_var($booleable, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
    }
}

if (!function_exists('prepare_response')) {
    /**
     * Prepare HTTP response Array
     *
     * @param $booleable
     * @return array
     */
    function prepare_response($type, $message)
    {
        return [["type" => $type, "message" => $message]];
    }
}

if (!function_exists('prepare_date_format')) {
    /**
     * Prepare Date format for storing in database
     * @param $date
     * @return false|string
     */
    function prepare_date_format($date)
    {
        return date("Y-m-d", strtotime($date));
    }
}

if (!function_exists('convert_to_currency')) {
    /**
     * Prepare Date format for storing in database
     * @param $date
     * @return false|string
     */
    function convert_to_currency($amount)
    {
        $fmt = numfmt_create('de_DE', NumberFormatter::CURRENCY);
        return numfmt_format_currency($fmt, $amount, "EUR");
    }
}
