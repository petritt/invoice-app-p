<?php

namespace App\Jobs;

use App\Models\Client;
use App\Models\Invoice;
use App\Notifications\InvoiceMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class SendInvoice implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $invoice;
    protected $client;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Invoice $invoice, Client $client)
    {
        $this->invoice = $invoice;
        $this->client = $client;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $pdf = $this->invoice->pdf();

        $relativePath = "\invoices" . DIRECTORY_SEPARATOR . "{$this->invoice->invoice_number}.pdf";
        Storage::disk('public')->put($relativePath, $pdf->output());

        $this->client->notify(new InvoiceMail($this->invoice));
    }
}
