<?php

namespace App\Notifications;

use App\Models\Invoice;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class InvoiceReminder extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var Invoice
     */
    protected $invoice;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Zahlungserinnerung')
            ->greeting('Hallo,')
            ->line("ich wollte Sie nur kurz daran erinnern, dass {$this->invoice->invoice_total} in Bezug auf unsere Rechnung {$this->invoice->invoice_number} Zahlung fällig ist.")
            ->line('Ich wäre Ihnen sehr dankbar, wenn Sie bestätigen könnten, dass alles auf dem richtigen Weg zur Zahlung ist.')
            ->salutation(new HtmlString("Vielen Grüße,<br />" . config('constants.customer.name')))
            ->attach($path = realpath($this->invoice->downloadPath()));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
