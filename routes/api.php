<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/clients', [\App\Http\Controllers\ClientsController::class, 'index']);
Route::get('/getProducts', [\App\Http\Controllers\ProductsController::class, 'index']);
Route::get('/clients/byClientId/{clientId}', [\App\Http\Controllers\ClientsController::class, 'getClientByid']);
Route::get('/activities/byClient/{clientId}/', [\App\Http\Controllers\ClientsController::class, 'getActivityByClient']);
Route::get('/getactivityByClient/{clientId}/', [\App\Http\Controllers\ClientsController::class, 'show']);
Route::get('/invoice/pdf/{invoiceId}', [\App\Http\Controllers\InvoicesController::class, 'generatePDF']);
Route::get('/invoice/byId/{invoiceId}', [\App\Http\Controllers\InvoicesController::class, 'edit']);
Route::get('/lastInvoiceNumber', [\App\Http\Controllers\InvoicesController::class, 'lastInvoiceNumber']);
Route::get('/send/invoice/{invoiceId}', [\App\Http\Controllers\InvoicesController::class, 'getInvoiceAsPdf']);
Route::get('/send/remainder/invoice/{invoiceId}', [\App\Http\Controllers\InvoicesController::class, 'sendReminder']);
Route::get('/invoices', [\App\Http\Controllers\InvoicesController::class, 'index']);
Route::get('/availablePrices', [\App\Http\Controllers\PricesController::class, 'index']);
Route::get('/dashboardData', [\App\Http\Controllers\HomeController::class, 'getDashboardData']);

