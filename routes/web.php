<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/{any}', function () {
    return view('home');
})->where('any', '.*');

Route::get('/client/{id}', [\App\Http\Controllers\ClientsController::class, 'show']);
Route::post('/client/store', [\App\Http\Controllers\ClientsController::class, 'store']);
Route::post('/activities/store', [\App\Http\Controllers\ActivitiesController::class, 'store']);
Route::post('/invoice/store', [\App\Http\Controllers\InvoicesController::class, 'store']);



