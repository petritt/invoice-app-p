<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Firma / Frau Enisa Ramadan</title>
    <style>
        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        body {
            font-family: DejaVu Sans, sans-serif;
        }
        h1,h2,h3,h4,h5,h6,p,span,div {
            font-size:13px;
            font-weight: normal;
        }
        th,td {
            font-size:13px;
        }
        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }
        .panel {
            margin-bottom: 20px;
            background-color: #fff;
            border: 1px solid transparent;
            border-radius: 4px;
            -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
            box-shadow: 0 1px 1px rgba(0,0,0,.05);
        }
        .panel-default {
            border-color: #ddd;
        }
        .panel-body {
            padding: 15px;
        }
        table {
            width: 100%;
            max-width: 100%;
            margin-bottom: 0;
            border-spacing: 0;
            border-collapse: collapse;
            background-color: transparent;
        }
        thead  {
            text-align: left;
            display: table-header-group;
            vertical-align: middle;
        }
        th, td  {
            border: 1px solid #ddd;
            padding: 6px;
        }
        .well {
            min-height: 20px;
            padding: 19px;
            margin-bottom: 20px;
            background-color: #f5f5f5;
            border: 1px solid #e3e3e3;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
        }
        .alert-heading {
            font-size: 52px;
            color: gray;
        }
        .client_details {
            padding: 40px 0 0;
        }
        .spacer {
            padding: 40px 0 0;
        }
        footer {
            position: fixed;
            bottom: 0;
        }
    </style>

</head>
<body>
<header>
    <div style="position:absolute; left:0; width:250pt;">
        <h1><b>{{ config('constants.customer.name') }}</b> <br />{{ config('constants.customer.occupation') }}</h1>
        <ul style="padding-bottom: 20px">
            <li>{{ config('constants.customer.address') }}</li>
            <li>{{ config('constants.customer.zip') }}<</li>
            <li>{{ config('constants.customer.phone') }}<</li>
        </ul>
    </div>
    <div style="margin-left:300pt;">
        <b>RECHNUNGSDATUM: </b> {{ $invoice['invoice_due_date'] }}<br />
        <b>RECHNUNGS-NR #: </b>  {{ $invoice['invoice_number'] }}
        <br />
    </div>
    <br />
</header>
<main>
    <div style="clear:both; position:relative; margin-top: 20px">
        <div style="position:absolute; left:0; width:250pt;">
            <h4 class="client_details">An:</h4>
            <div class="panel panel-default">
                <div class="panel-body">
                    <ul>
                        <li>{{ $invoice['client'][0]['client_name'] }}</li>
                        <li>{{ $invoice['client'][0]['address'] }}</li>
                        <li>{{ $invoice['client'][0]['zip'] }}, {{ $invoice['client'][0]['city']  }}</li>
                    </ul>
                </div>
            </div>
        </div>
        <div style="margin-left: 300pt;">
            <h2 class="alert-heading">Rechnung</h2>
        </div>
    </div>
    <table class="table table-bordered spacer">
        <thead>
        <tr>
            <th>Bezeichnung</th>
            <th>Menge</th>
            <th>Preis/Einh</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($invoice['invoice_activity'] as $activity)
            <tr>
                <td>{{ isset($activity['product_name']) ? $activity['product_name'] : 'Haushaltshilfe' }} {{ $activity['date'] }}</td>
                <td>{{ $activity['duration'] }} {{ !isset($activity['product_name']) || $activity['product_name'] === 'Haushaltshilfe' ? 'Stunden' : '' }}</td>
                <td>{{ isset($activity['product_price']) ? convert_to_currency($activity['product_price']) : convert_to_currency($invoice['client'][0]['price']) }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div style="clear:both; position:relative;">
        <div style="margin-left: 300pt;">
            <h4><b>Rechnungsbetrag: {{ $invoice['invoice_total'] }}</b></h4>
        </div>
    </div>
    <br /><br />
    <div class="well">
        <p><b>Nach § 19 Abs. 1 UStG wird keine Umsatzsteuer berechnet.</b></p>
        <p>Die Rechnung ist sofort fällig. Bitte überweisen Sie den Rechnungsbetrag ohne Abzüge auf unser Bankkonto.</p>
    </div>
</main>

<footer>
    <p style="font-size: 10px">{{ config('constants.customer.payment_details') }}</p>
</footer>
</body>
</html>
