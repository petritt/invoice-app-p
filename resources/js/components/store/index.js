import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);
import product from './modules/product'
import activity from './modules/activity'
import invoice from './modules/invoice'
import client from './modules/client'
import dashboard from './modules/dashboard'

export default new Vuex.Store({
    modules: {
        activity,
        product,
        invoice,
        client,
        dashboard
    },
    strict: false,
});
