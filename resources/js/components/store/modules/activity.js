const getClientsUrl = '/api/clients';

// initial state
const state = {
    clients: [],
};

// getters
const getters = {

};

// actions
const actions = {
    LoadClients({commit}) {
        return new Promise((resolve, reject) => {
            axios.get(getClientsUrl).then((response) => {
                commit('SetClients', response.data);

                resolve(response);
            }).catch((err) => {
                reject(err)
            })
        });
    }
};

// mutations
const mutations = {
    SetClients(state, clients) {
        state.clients = clients
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
