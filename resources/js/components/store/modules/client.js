const availablePricesURL = '/api/availablePrices';

// initial state
const state = {
    available_prices: [],
};

// getters
const getters = {

};

// actions
const actions = {
    loadAvailablePrices({commit}) {
        return new Promise((resolve, reject) => {
            axios.get(availablePricesURL).then((response) => {
                commit('SetAvailablePrices', response.data);

                resolve(response);
            }).catch((err) => {
                reject(err)
            })
        });
    },
};

// mutations
const mutations = {
    SetAvailablePrices(state, available_prices) {
        state.available_prices = available_prices;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
