const getProductsURL = '/api/getProducts';

// initial state
const state = {
    products: []
};

// getters
const getters = {

};

// actions
const actions = {
    LoadProducts({commit}) {
        return new Promise((resolve, reject) => {
            axios.get(getProductsURL).then((response) => {
                commit('SetProducts', response.data);

                resolve(response);
            }).catch((err) => {
                reject(err)
            })
        });
    },
};

// mutations
const mutations = {
    SetProducts(state, products) {
        state.products = products;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
