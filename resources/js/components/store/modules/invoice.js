const lastInvoiceNumberUrl = '/api/lastInvoiceNumber';
const getInvoicesUrl = '/api/invoices';

// initial state
const state = {
    last_invoice_number: null,
    invoices: []
};

// getters
const getters = {

};

// actions
const actions = {
    getLastInvoiceNumber({commit}) {
        return new Promise((resolve, reject) => {
            axios.get(lastInvoiceNumberUrl).then((response) => {
                commit('SetInvoiceNumber', response.data);

                resolve(response);
            }).catch((err) => {
                reject(err)
            })
        });
    },
    LoadInvoices({commit}) {
        return new Promise((resolve, reject) => {
            axios.get(getInvoicesUrl).then((response) => {
                commit('SetInvoices', response.data);

                resolve(response);
            }).catch((err) => {
                reject(err)
            })
        });
    }
};

// mutations
const mutations = {
    SetInvoiceNumber(state, invoice_number) {
        state.last_invoice_number = invoice_number
    },
    SetInvoices(state, invoice) {
        state.invoices = invoice;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
