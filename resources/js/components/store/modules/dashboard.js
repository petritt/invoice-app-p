const dashboardReportURL = '/api/dashboardData';

// initial state
const state = {
    dashboard_report: [],
};

// getters
const getters = {};

// actions
const actions = {
    loadDashboardReports({commit}) {
        return new Promise((resolve, reject) => {
            axios.get(dashboardReportURL).then((response) => {
                commit('setDashboardReport', response.data);

                resolve(response);
            }).catch((err) => {
                reject(err)
            })
        });
    },
};

// mutations
const mutations = {
    setDashboardReport(state, dashboard_report) {
        state.dashboard_report = dashboard_report;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
