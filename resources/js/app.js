import Vue from "vue";
import Vuex from 'vuex'
import Router from 'vue-router'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import '@fortawesome/fontawesome-free/js/all.js';
import Vuelidate from 'vuelidate'

require('./bootstrap');
window.Vue = require('vue').default;

Vue.use(Router);
Vue.use(Vuex);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(Vuelidate);

Vue.mixin({
    methods: {
        handleHttpError(err, SatType = false) {
            if (err.message === "Network Error") {
                this.$root.$children[0].$refs.messageComponent.showMessages([{
                    "type": "danger",
                    "message": "Keine Internetverbindung \n"
                }], "Ein Fehler ist aufgetreten", this.$route.path)
            } else if (err.response.status === 401) {
                this.$root.$children[0].$refs.messageComponent.showMessages([{
                    "type": "danger",
                    "message": "Ihre Sitzung ist abgelaufen. Bitte melden Sie sich erneut an.\n"
                }], "Ein Fehler ist aufgetreten", '/login')
            } else if (err.response.status === 403) {
                this.$root.$children[0].$refs.messageComponent.showError(err);
            } else {
                this.$root.$children[0].$refs.messageComponent.showError(err);
            }
        },
        showMessages(messages, title, redirectUrl, redirectId) {
            this.$root.$children[0].$refs.messageComponent.showMessages(messages, title, redirectUrl, redirectId);
        },
    },
});

Vue.filter('toCurrency', function (value) {
    if (typeof value !== "number") {
        return value;
    }
    let formatter = new Intl.NumberFormat('de-DE', {
        style: 'currency',
        currency: 'EUR'
    });
    return formatter.format(value);
});

import store from './components/store/index'
import Index from "./components/index.vue";
import Dashboard from "./components/Dashboard.vue";
import Client from "./components/Client/Client.vue";
import NewClient from "./components/Client/NewClient.vue";
import EditClient from "./components/Client/EditClient"
import NewActivity from "./components/Activities/NewActivity.vue";
import Activity from "./components/Activities/Activity.vue";
import Invoice from "./components/Invoices/Invoice.vue";
import NewInvoice from "./components/Invoices/NewInvoice.vue";
import EditInvoice from "./components/Invoices/editInvoice.vue";
import Product from "./components/Products/Product.vue";
import ClientReport from "./components/Reports/ClientReport";

const router = new Router({
    mode: 'history',
    linkExactActiveClass: 'nav_active',
    routes: [
        {
            path: '/dashboard',
            name: 'Dashboard',
            component: Dashboard,
        },
        {
            path: '/client',
            name: 'Client',
            component: Client,
        },
        {
            path: '/client/add',
            name: 'NewClient',
            component: NewClient,
        },
        {
            path: '/client/:client_id/edit',
            name: 'EditClient',
            component: EditClient,
        },
        {
            path: '/client/report',
            name: 'ClientReport',
            component: ClientReport,
        },
        {
            path: '/activity/',
            name: 'Activity',
            component: Activity,
        },
        {
            path: '/activity/:client_id/add',
            name: 'NewActivity',
            component: NewActivity,
        },
        {
            path: '/activity/show',
            name: 'showActivity',
            component: Activity,
        },
        {
            path: '/invoice/',
            name: 'Invoice',
            component: Invoice,
        },
        {
            path: '/invoice/add',
            name: 'NewInvoice',
            component: NewInvoice,
        },
        {
            path: '/invoice/:invoice_id/edit',
            name: 'EditInvoice',
            component: EditInvoice,
        },
        {
            path: '/product/',
            name: 'Product',
            component: Product,
        },
    ],
});

new Vue({
    el: '#app',
    store,
    render: h => h(Index),
    router,
});
